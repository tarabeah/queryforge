<?php namespace Icore\Queryforge;

class DB{

    private static $connections = [];
    private static $PDOSingleton = [];
    private $allPDOConnections = [];
    private static $countOfBeginTransaction = 0;

    /**
     * @param $name string: name of connection
     * @param $dbType: database type: MySQL, PostgreSQL, Sqlite ... etc
     * @param $credential: Database connection credential.
     * @return object QueryForge
     */
    public function define($name = '', $dbType = null, $credential = null){

        if(isset(self::$connections[$name])){
            return self::$PDOSingleton[$name];
        }

        return self::$connections[$name] = [
            'Error'  => false,
            'dbType' => $dbType,
            'credential'    => $credential
        ];
    }

    /**
     * Connect to sqlite
     * @param $credential
     * @return \PDO
     */
    public static function sqlite($credential){
        return new \PDO("sqlite:" . $credential);
    }

    /**
     * Connect to mysql
     * @param $credential
     * @return \PDO
     */
    public static function mysql($credential){
        return new \PDO($credential[0], $credential[1], $credential[2]);
    }

    static function getFluent($name = ''){
        if(!array_key_exists($name, self::$connections)){
            throw new \Exception("Connection not defined: " . __LINE__);
        }

        $return['fluent'] = new \Envms\FluentPDO\Query(self::getPDO($name));
        return (object) $return;
    }

    /**
     * Return PDO based on database type, it could be PDO for mysql, sql, sqlite .. etc
     *
     * @param $name : name of connection
     * @return PDO object
     */
    static function getPDO($name){

        if(isset(self::$PDOSingleton[$name])){
            return self::$PDOSingleton[$name];
        }

        $connData = self::$connections[$name];

        $dbType = $connData['dbType'];
        $credential = $connData['credential'];

        try {
            $pdo = self::$dbType($credential);

            // This pdo created during running the transactions:
            if(self::$countOfBeginTransaction){
                $pdo->beginTransaction();
            }
        } catch (\Exception $e){
            return [
                'Error'  => true,
                'Message' => $e->getMessage()
            ];
        }


        return self::$PDOSingleton[$name] = &$pdo;
    }

    /**
     * Start transaction for all opened PDO
     * @return false
     */
    static function transStart(){

        // if the transaction is already enabled, so there is no need to start it again.
        if(self::$countOfBeginTransaction){
            self::$countOfBeginTransaction++;
            return false;
        }

        self::$countOfBeginTransaction = 1;
        if(self::$PDOSingleton){
            foreach(self::$PDOSingleton as $pdo){
                $pdo->beginTransaction();
            }
        }
    }

    /**
     * Commit transaction for all opened PDO
     * @return bool
     */
    static function commit(){

        if(self::$countOfBeginTransaction){
            self::$countOfBeginTransaction--;

            if(self::$countOfBeginTransaction != 0) {
                return false;
            }
        }

        if(self::$PDOSingleton){
            foreach(self::$PDOSingleton as $pdo){
                $pdo->commit();
            }
        }

        return true;
    }

    /**
     * Commit transaction for all opened PDO
     *
     * @return bool
     */
    static function rollBack(){
        self::$countOfBeginTransaction--;

        if(self::$countOfBeginTransaction){
            return false;
        }

        if(self::$PDOSingleton){
            foreach(self::$PDOSingleton as $pdo){
                $pdo->rollBack();
            }
        }

        return true;
    }

    /**
     * Get all PDO connections
     */
    static public function getPDOs(){
        return self::$PDOSingleton;
    }
}
