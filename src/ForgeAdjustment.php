<?php namespace Icore\Queryforge;

use PHPUnit\Runner\Exception;

class ForgeAdjustment {

    private $relatedKeys = [
        'where'     => ['or', 'and'],
        'innerJoin' => ['as', 'on']
    ];

    private $fluentParams = [];

    /**
     * @param $queryStructure: method and elements that used to build the
     * @return array
     */
    public function get($queryStructure){
        $queryStructure = $this->prepareQueryStructureForMainMethods($queryStructure);
        $queryStructure = $this->buildStatementForRelatedMethods($queryStructure);
        return $queryStructure;
    }

    /**
     * @param $queryStructure: The sequence of methods and its parameter as called.
     * @return array
     */
    function prepareQueryStructureForMainMethods($queryStructure){

        $return = [];
        $mainOperator = ''; // like (where), innerJoin or any operater that could be followed with Follower operator
        $operatorFollowerNames = []; // like : and, or ... atc, Follower operators are the operators that could be followed to $mainOperator
        $operatorFollowersData = []; // id = 1, name = ahmad
        $queryStructure[] = ['method' => 'EOQ', 'param' => 'EOQ'];

        $previousSegment = '';
        foreach($queryStructure as $call) {

            $segment = $call['method'];
            $values  = $call['param'];

            // Some times when create query in Model, we use "and" before "where" so
            // in similar cases we have to change "and" to "where"
            if(in_array($segment, array('and', 'or')) and $previousSegment != 'where') {
                $segment = 'where';
            }

            // if there are "where" after "where", so we suppose that the
            // developer mean it should be "and":
            if($segment == 'where' and $previousSegment == 'where'){
                $segment = 'and';
            }

            // Check if this is origin to fluentPdo object or should be custom by QueryForge
            if (array_key_exists($segment, $this->relatedKeys)) {

                // if there is value in $mainOperator, that mean that there is one $mainOperator before
                // this one and it is closed new, so we have to build it:
                if($mainOperator != ''){
                    $return[][$mainOperator] = $operatorFollowersData;
                    $operatorFollowersData = [];
                }

                $mainOperator = $segment;
                $operatorFollowerNames = $this->relatedKeys[$segment];
                $operatorFollowersData[] = array('method' => $segment, 'values' => $values);

                $previousSegment = $segment;
                continue;
            }

            // if this method name relate to custom QueryForge followers
            // add it to the followers structure keywords data
            if (in_array($segment, $operatorFollowerNames)) {
                $operatorFollowersData[] = array('method' => $segment, 'values' => $values);
                $previousSegment = $segment;
                continue;
            }

            // if this is first round after QueryForge, set updated embedded query structure.
            if ($mainOperator != '' && !in_array($segment, $operatorFollowerNames)) {

                // set main operator, and its own follow, for example
                // the main Operator could be where and the follows are (and, or).
                $return[][$mainOperator] = $operatorFollowersData;

                $mainOperator = '';
                $operatorFollowerNames = [];
                $operatorFollowersData = [];

                // now the mainOperator and its follow are closed, but the current segment
                // should be added:
                if($values == 'EOQ'){
                    break;
                }

                $return[][$segment] = $values;

            } else if ($values != 'EOQ') {
                $return[][$segment] = $values;
            }
        }

        $previousSegment = $segment;
        return $return;
    }

    /**
     * @param $queryStructure
     * @return array
     */
    function buildStatementForRelatedMethods($queryStructure){

        $return = [];
        foreach($queryStructure as $method => $relatedMethods) {

            $method = key($relatedMethods);
            $relatedMethods = $relatedMethods[key($relatedMethods)];

            // if this is fluentPdo method pass it as is.
            if(!method_exists($this, $method)){
                $return[] = array('method' => $method, 'param' => $relatedMethods);
                continue;
            }

            // This is queryForge custom method:
            $fluentPdoCall = $this->$method($relatedMethods);
            $return = array_merge($return, $fluentPdoCall);
        }

        return $return;
    }

    /**
     * @param $relatedMethods
     * @return array
     */
    protected function where($relatedMethods){

        $strQuery = '';
        $valuesParameters = [];
        foreach($relatedMethods as $methodCall){

            // The FLuendPDO already use array as "in" so there is no need to create customization for it.
            if(isset($methodCall['values'][2]) && strtolower($methodCall['values'][2]) == "in"){
                unset($methodCall['values'][2]);
            }

            if(count($methodCall['values']) != 3){
                $return[] = array('method' => 'where', 'param' => $methodCall['values']);
                continue;
            }

            $column = $methodCall['values'][0];
            $values = $methodCall['values'][1];
            $operatorName = $methodCall['values'][2];

            $query = $this->_queryString($column, $operatorName, $values);
            $valuesParameters = array_merge($valuesParameters, $query[1]);

            $operatorName = $methodCall['method'] == 'where' ? '' : $methodCall['method'];
            $strQuery .= strtoupper($operatorName) . ' ' . $query[0];
        }


        $return[] = array('method' => 'where', 'param' => array($strQuery, $valuesParameters));
        return $return;
    }

    /**
     * @param $relatedMethods
     * @return array
     */
    protected function select($relatedMethods){

        $return = [];
        if(is_array($relatedMethods)){
            $return[] = array('method' => 'select', 'param' => array(null));
            $return[] = array('method' => 'select', 'param' => array($relatedMethods));
        } else {
            $return[] = array('method' => 'select', 'param' => array($relatedMethods));
        }

        return $return;
    }

    /**
     * @param $relatedMethods
     * @return array
     */
    protected function innerJoin($relatedMethods){
        return $this->_join($relatedMethods, 'innerJoin');
    }

    /**
     * @param $relatedMethods: the sequence of call methods
     * @param $joinType: leftJoin, rightJoin, innerJoin
     * @return array
     */
    protected function _join($relatedMethods, $joinType){

        $relatedMethods = $this->_joinSiplitter($relatedMethods, $joinType);

        $return = [];
        foreach($relatedMethods as $counter => $call){

            $strQuery = '';

            foreach($call as $callElement){
                switch (true){
                    case $joinType === $callElement[0]:
                        $strQuery = $callElement[1][0] . ' ';
                        break;

                    case "as" === $callElement[0]:
                        $strQuery .= 'AS ' . $callElement[1][0] . ' ';
                        break;

                    case $callElement[0] == 'on':
                        $strQuery .= 'ON ' . $callElement[1][0] . ' ' . $callElement[1][2] . ' ' . $callElement[1][1];
                        break;
                }
            }

            $return[] = ['method' => $joinType, 'param' => [$strQuery]];
        }

        return $return;
    }

    /**
     * if there are many join operators, split them to with followers.
     *
     * @param $relatedMethods
     * @param $joinType
     * @return array
     */
    protected function _joinSiplitter($relatedMethods, $joinType){

        $relatedMethods[] = ['method' => 'EOQ', 'values' => 'EOQ'];
        $splitter = [];
        $subQueryStructure = [];

        foreach($relatedMethods as $counter => $methodCall){

            $method = $methodCall['method'];
            $param = $methodCall['values'];

            if($method == $joinType || $method == 'EOQ'){
                if($subQueryStructure){
                    $splitter[] = $subQueryStructure;
                }

                $subQueryStructure = [];
            }

            if($param == 'EOQ'){
                break;
            }

            $subQueryStructure[] = [$method, $param];
        }

        return $splitter;
    }

    /**
     * @param $column string
     * @param $operatorName string
     * @param $values array
     * @return array
     */
    private function _queryString($column, $operatorName, $values){

        $operatorName = strtoupper($operatorName);
        if(is_array($values)){
            $queryString = $column . ' ' . $operatorName . '';
            $queryString .= ' (' . trim(str_repeat(' ?,', count($values)), ',') . ') ';
        } else {
            $queryString = $column . ' ' . $operatorName . ' ? ';
            $values = [$values];
        }

        return [$queryString, $values];
    }
}
