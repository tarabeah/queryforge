<?php namespace Icore\Queryforge;

class Query
{
    private $connectionName = null;
    private $queryTypeMethods = [
        'insertInto' => "Insert",
        'from' => 'Select',
        'delete' => 'Delete',
        'deleteFrom' => 'Delete',
        'update' => 'Update'
    ];

    private $methodReplacement = [
        'delete' => 'deleteFrom'
    ];

    private $queryType = null; // Select, Delete, Update, Insert
    private $queryStructure = [];
    private static $queryString;

    /**
     * Qry constructor: name of connection name that defined in Icore\Queryforge\DB
     * @param string $connectionName
     */
    function __construct($connectionName = '')
    {
        $this->connectionName = $connectionName;
    }

    /**
     * @param $name
     * @param $arguments
     * @return $this
     */
    function __call($name, $arguments)
    {
        $this->queryStructure[] = ['method' => $name, 'param' => $arguments];

        return $this;
    }

    /**
     * Get Fluent PDO object after building.
     * @return mixed
     * @throws \Exception
     */
    function getFluent()
    {
        $query = \Icore\Queryforge\DB::getFluent($this->connectionName);
        $query = $query->fluent;
        $this->queryType = null;

        $forgeAdjustment = new \Icore\Queryforge\ForgeAdjustment();
        $queryStructure = $forgeAdjustment->get($this->queryStructure);

        foreach ($queryStructure as $call) {

            $method = $call['method'];
            $args = $call['param'];

            if ($this->queryType == null && in_array($method, array_keys($this->queryTypeMethods))) {
                $this->queryType = $this->queryTypeMethods[$method];
            }

            if($method == 'limit' && isset($args[1])) {
                $query->limit($args[1])->offset($args[0]);
            } else {
                $method = isset($this->methodReplacement[$method]) ? $this->methodReplacement[$method] : $method;
                $query = call_user_func_array(array($query, $method), $args);
            }
        }

        return $query;
    }

    /**
     * Execute query and return result
     *
     * @return mixed
     * @throws \Exception
     */
    function exec()
    {
        $query = $this->getFluent();
        $this->setLastQueryString($query);
        $result = 0;

        switch ($this->queryType) {
            case "Select":
                try {
                    $result = $query->fetchAll();
                } catch (\PDOException $ex) {
                    throw new QueryException($ex, $query);
                }

                return $result;
            case "Delete":
            case "Update":
            case "Insert":

                try {
                    $result = $query->execute();
                } catch (\PDOException $ex) {
                    throw new QueryException($ex, $query);
                }

                return $result;
        }
        return $result;
    }

    /**
     * Set QueryString for last  is posted to the DB engine
     * @param $queryFluentPDO
     */
    private function setLastQueryString($queryFluentPDO){
        // Set the QueryString and QueryParams as received from FluentPDO
        $baseQueryString = $queryFluentPDO->getQuery();
        $baseQueryParam = $queryFluentPDO->getParameters();

        static::$queryString = $baseQueryString;

        // Replace the "?" parameters in the queryString of FluentPDO with its value:
        if($baseQueryParam){
            foreach($baseQueryParam as $parameter){
                if(!is_int($parameter)){
                    $parameter = str_replace('"', '\"', $parameter);
                    $parameter = '"' . $parameter . '"';
                }

                // only replaces the first occurrence of "?" with current parameter
                static::$queryString = implode($parameter, explode('?', static::$queryString , 2));
            }
        } else {
            static::$queryString = $baseQueryString;
        }
    }

    /**
     * Return the last query that executed by this utility
     * @return mixed
     */
    static public function getLastQuery(){
        return static::$queryString;
    }
}
